package com.shijie.servicehi;

import com.shijie.fallback.ServiceHiClientHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by shijie on 2017/8/24 0024.
 */
@FeignClient(value = "service-hi", fallback = ServiceHiClientHystrix.class)
public interface ServiceHiClient {

    @RequestMapping(method = RequestMethod.GET, value = "/add")
    Integer add(@RequestParam(value = "a") Integer a, @RequestParam(value = "b") Integer b);

}
