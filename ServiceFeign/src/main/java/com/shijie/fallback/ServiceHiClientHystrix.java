    package com.shijie.fallback;

    import com.shijie.servicehi.ServiceHiClient;
    import org.springframework.stereotype.Component;
    import org.springframework.web.bind.annotation.RequestParam;

    /**
     * Created by shijie on 2017/8/25 0025.
     */
    @Component
    public class ServiceHiClientHystrix implements ServiceHiClient {

        @Override
        public Integer add(@RequestParam(value = "a") Integer a, @RequestParam(value = "b") Integer b) {
            return -4;
        }
    }
